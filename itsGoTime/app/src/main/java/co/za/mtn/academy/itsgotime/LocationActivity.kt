package co.za.mtn.academy.itsgotime

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import com.birjuvachhani.locus.Locus
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions

class LocationActivity : AppCompatActivity(), OnMapReadyCallback {
    private var googleMap: GoogleMap? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_location)
        // get map fragment
        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        Locus.startLocationUpdates(this) { result ->
            result.location?.let {
                val mtnSite = LatLng(result.location!!.latitude, result.location!!.longitude)
                googleMap?.addMarker(MarkerOptions().position(mtnSite).title("Mukondleteri test 1"))
            }
            result.error?.let {
            }
        }
    }

    override fun onMapReady(map: GoogleMap) {
        this.googleMap = map

        val mtnSite = LatLng(-34.0, 151.0)
        googleMap?.moveCamera(CameraUpdateFactory.newLatLng(mtnSite))
    }
}