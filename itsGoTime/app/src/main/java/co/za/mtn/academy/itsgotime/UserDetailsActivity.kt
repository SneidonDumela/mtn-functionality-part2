package co.za.mtn.academy.itsgotime

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import co.za.mtn.academy.itsgotime.core.model.User
import com.android.billingclient.api.*
import com.android.billingclient.api.BillingClient.SkuType.INAPP
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.google.android.gms.ads.MobileAds
import com.squareup.picasso.Picasso

class UserDetailsActivity : AppCompatActivity(), PurchasesUpdatedListener {
    private var billingClient: BillingClient? = null
    private var bannerView: AdView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_details)

        // Link text view to code
        val textView = findViewById<TextView>(R.id.textView)
        val imageView = findViewById<ImageView>(R.id.profileImage)

        val takePicture = findViewById<Button>(R.id.takePicture)
        //val shareProfile = findViewById<Button>(R.id.shareProfile)
        //val playSound = findViewById<Button>(R.id.playSound)
        val getLocation = findViewById<Button>(R.id.getLocation)
        val buyProduct = findViewById<Button>(R.id.buyProduct)

        bannerView = findViewById<AdView>(R.id.adView)

        //
        MobileAds.initialize(this)

        val adRequest = AdRequest.Builder().build()
        bannerView?.loadAd(adRequest)

        billingClient = BillingClient.newBuilder(this).enablePendingPurchases().setListener(this).build()

        // get data
        val user = intent.getParcelableExtra<User>("User")

        // add user name to text view
        textView.text = user?.name
        // add user profile using picasso
        Picasso.get().load(user?.profileUrl).into(imageView)

        // set onclick listeners
        takePicture.setOnClickListener {
            val intent = Intent(this, TakePictureActivity::class.java)
            startActivity(intent)
        }

        buyProduct.setOnClickListener {
            this.purchase()
        }

        /*
        shareProfile.setOnClickListener {
            if (user != null) {
                this.shareProfile(user)
            }
        }

        playSound.setOnClickListener {
            val intent = Intent(this, AudioPlayActivity::class.java)
            startActivity(intent)
        }

        getLocation.setOnClickListener {
            val intent = Intent(this, LocationActivity::class.java)
            startActivity(intent)
        }
         */

        getLocation.setOnClickListener {
            val intent = Intent(this, LocationActivity::class.java)
            startActivity(intent)
        }
    }

    fun shareProfile (user: User) {
        val sendIntent: Intent = Intent().apply {
            action = Intent.ACTION_SEND
            putExtra(Intent.EXTRA_TEXT, user.name)
            type = "text/plain"
        }

        val shareIntent = Intent.createChooser(sendIntent, null)
        startActivity(shareIntent)
    }


    fun purchase() {
        if (billingClient!!.isReady) {
            //initiatePurchase()
        } else {
            billingClient = BillingClient.newBuilder(this).enablePendingPurchases().setListener(this).build()
            billingClient!!.startConnection(object : BillingClientStateListener {
                override fun onBillingSetupFinished(billingResult: BillingResult) {
                    if (billingResult.responseCode == BillingClient.BillingResponseCode.OK) {
                        initiatePurchase()
                    } else {
                        Toast.makeText(applicationContext, "Error " + billingResult.debugMessage, Toast.LENGTH_SHORT).show()
                    }
                }
                override fun onBillingServiceDisconnected() {

                }
            })
        }
    }


    private fun initiatePurchase() {
        val skuList: MutableList<String> = ArrayList()
        skuList.add("its_go_time_pro")

        val params = SkuDetailsParams.newBuilder()
        params.setSkusList(skuList).setType(INAPP)

        billingClient!!.querySkuDetailsAsync(params.build()) { billingResult, skuDetailsList ->
            if (billingResult.responseCode == BillingClient.BillingResponseCode.OK) {
                if (skuDetailsList != null && skuDetailsList.size > 0) {
                    val flowParams = BillingFlowParams.newBuilder().setSkuDetails(skuDetailsList[0]).build()
                    billingClient!!.launchBillingFlow(this@UserDetailsActivity, flowParams)
                } else {
                    Toast.makeText(applicationContext, "Purchase Item not Found", Toast.LENGTH_SHORT).show()
                }
            } else {
                Toast.makeText(applicationContext, " Error " + billingResult.debugMessage, Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onPurchasesUpdated(billingResult: BillingResult, purchases: MutableList<Purchase>?) {

        bannerView?.visibility = View.GONE
    }
}